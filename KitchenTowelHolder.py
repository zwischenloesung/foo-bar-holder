########################################################################
# This tool generates a 3D body that can be used as a beam holder
# hook, e.g. for kitchen paper towels or cleaning paper towels in
# a 3D-printer room... (I wrote it mostly to understand how CadQuery
# works.)
# I had an aluminum rod laying around which became the reference. You
# can simply adapt height, width, thickness, bar_diameter and the
# screw size to your needs, the rest should be calculated automagically.
########################################################################
# note: see https://github.com/CadQuery/cadquery
# author/copyright: <michael.lustenberger@inofix.ch>
# license: gladly sharing with the universe and any lifeform,
#          be it naturally born or constructed, alien or kin..
#          USE AT YOUR OWN RISK.
########################################################################

import cadquery as cq
## today the cq-editor did not want to export to STL any more and as
## I started debugging I found the manual exporter, which is even
## nicer, I think.. To get it working, just uncomment this and the last
## last 'exporter' line below...
#from cadquery import exporters
import math

# the total heigth of the device 
height = 70

# the width of the device
width = 35

# the minimal thickness of the device
thickness = 10.0

# the thickness of the beam/bar that will be used
bar_diameter = 11.6

# what screws will be used
screw_diameter = 4.5
screw_head_diameter = 6

# the proportions 
## the relation beween the hold backplate and the bar holder
hold_proportion = 1.5
## the relation between foot (wall mount) and back (stand)
fix_proportion = 1.4

hold_height = height / 2
hold_round_edge_diameter = hold_height / 4

# move the hole to intersect the border of the hold
bar_hole_displacement = (hold_height/hold_proportion-hold_height)/2
# pythagoras helps to get the radius from the chord
bar_hole_diameter = 2*math.sqrt(math.pow(bar_diameter/2, 2)+math.pow(bar_hole_displacement,2))

# the reference to work on
wp = cq.Workplane("XY")

# the bar holder,
## hold0 is part of the backplate
hold0 = wp\
    .box(hold_height, width, thickness)\
    .edges("|Z").fillet(hold_round_edge_diameter)
## hold1 is where the bar is placed
hold1 = wp\
    .box(hold_height/hold_proportion, width, thickness/fix_proportion)\
    .edges("|Z").fillet(hold_round_edge_diameter)\
    .translate((bar_hole_displacement,0,thickness/2+thickness/fix_proportion/2))\
    .faces(">Z").workplane().hole(bar_hole_diameter)
# the back plate between holder and wall mount part
## stand0 unites the hold with the fix, basically covering the round edges of hold0
stand0 = hold0.translate((hold_height/2,0,0))\
    .box(hold_height, width, thickness)
## stand1 is the connector between the fix and the hold
stand1 = stand0.translate((hold_height/2,0,0))\
    .box(hold_height, width, thickness)

# the wall mount
# rotate 90 degrees and displace twice the height minus half (as it was centered)
fix = wp.box(hold_height*fix_proportion, width, thickness)\
    .edges("|Z").fillet(1)\
    .rotate((0,1,0),(0,0,0),90)\
    .translate(((hold_height*1.5-thickness/2),0,hold_height*fix_proportion*.5))\
    .faces("-X").workplane()\
    .rect(hold_height*fix_proportion, 0, forConstruction=True)\
    .translate((0,width/4,hold_height*fix_proportion*.75))\
    .cboreHole(screw_diameter, screw_head_diameter, thickness/5)\
    .rect(hold_height*fix_proportion, 0, forConstruction=True)\
    .translate((-thickness/2-0.01,-width/4,-10))\
    .cboreHole(screw_diameter, screw_head_diameter, thickness/5)\

# union all solids
result = hold0.union(hold1).union(stand0).union(stand1).union(fix)

# render
show_object(result, options={"alpha":0.5, "color": (64, 164, 223)})

# export to STL
## uncomment to export directly
#exporters.export(result, './KitchenTowelHolder.stl')
